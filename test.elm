module Main exposing (main)

import Html exposing (..)
import Http
import Json.Decode as Decode
import Task
import Time


main : Program Never Model Msg
main =
    Html.program
        { init = init
        , view = view
        , update = update
        , subscriptions = (\_ -> Sub.none)
        }



-- MODEL


type alias Model =
    { info : Maybe InfoMessage
    }


type alias InfoMessage =
    { message : String
    , time : Time.Time
    }


init : ( Model, Cmd Msg )
init =
    ( Model Nothing, doRequest )



-- REQUESTS


doRequest : Cmd Msg
doRequest =
    Task.map2 (,) fetchToken Time.now
        |> Task.andThen secondRequest
        |> Task.attempt HandleResult


fetchToken : Task.Task Http.Error String
fetchToken =
    Decode.field "token" Decode.string
        |> Http.get "first.json"
        |> Http.toTask


secondRequest : ( String, Time.Time ) -> Task.Task Http.Error InfoMessage
secondRequest ( token, time ) =
    Decode.field "message" Decode.string
        |> Decode.map (\x -> InfoMessage x time)
        |> Http.get (token ++ ".json")
        |> Http.toTask



-- UPDATE


type Msg
    = HandleResult (Result Http.Error InfoMessage)


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        HandleResult (Err _) ->
            ( model, Cmd.none )

        HandleResult (Ok x) ->
            ( { model | info = Just x }, Cmd.none )



-- VIEW


view : Model -> Html Msg
view model =
    div []
        [ span []
            [ text <|
                case model.info of
                    Nothing ->
                        "no result yet"

                    Just { message, time } ->
                        message ++ " at " ++ (toString time)
            ]
        ]
